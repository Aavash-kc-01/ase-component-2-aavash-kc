﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicsProgramminApplication
{
    /// <summary>
    /// Form class
    /// </summary>
    public partial class Form1 : Form
    {
        /// <summary>
        /// variable decleration
        /// </summary>
        Boolean saveChange;  
        String filePath;

        Bitmap outputBitmap = new Bitmap(640, 480);

        /// <summary>
        /// Constructor
        /// </summary>
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// form load event method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">Gorm load event
        /// event</param>
        private void Form1_Load(object sender, EventArgs e)
        {
            outputBitmap = new Bitmap(640, 480);
        }

        /// <summary>
        /// run button click event to run the code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">event</param>
        private void btnExecute_Click(object sender, EventArgs e)
        {
            string cmm = txtCommand.Text.ToLower();
            string cod = txt_code.Text.ToLower();
            
            
                CommandParser cm = new CommandParser();
                string parsedCode= cm.Parser(cmm, cod);
                Graphics g = Graphics.FromImage(outputBitmap);
                g.DrawImageUnscaled(cm.GetBitmap(), 0, 0);
                pnl_canvas.Refresh();
                if (parsedCode == "clear")
                    {
                        CommandParser cl = new CommandParser();
                        g = Graphics.FromImage(outputBitmap);
                        g.Clear(Color.White);
                        txt_code.Clear();
                        Refresh();
                    }
                    
        }

        /// <summary>
        /// Save button click event to save the file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">click event</param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(saveChange == false)
            {
                //when there is no unsaved existing project
                try
                {
                    SaveFileDialog sf = new SaveFileDialog();
                    sf.Filter = "Text Document(*.txt)|*.txt|All Files(*.*)|*.*";
                    if (sf.ShowDialog() == DialogResult.OK)
                    {
                        System.IO.File.WriteAllText(sf.FileName, txt_code.Text);
                    }

                    filePath = Path.GetFullPath(sf.FileName);


                }
                catch (Exception)
                {
                    MessageBox.Show("File couldn't be saved");
                }
            }
            else
            {

                DialogResult dialogResult = MessageBox.Show("Do you want to save changes to existing project before creating new project.", "Unsaved Changes", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {

                    try
                    {
                        System.IO.File.WriteAllText(filePath, txt_code.Text);
                        saveChange = false;
                        MessageBox.Show("Saved Sucessfully");
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Error while saving file.");
                    }
                }
                try
                {
                    SaveFileDialog sf = new SaveFileDialog();
                    sf.Filter = "Text Document(*.txt)|*.txt|All Files(*.*)|*.*";
                    if (sf.ShowDialog() == DialogResult.OK)
                    {
                        System.IO.File.WriteAllText(sf.FileName, txt_code.Text);
                    }

                    filePath = Path.GetFullPath(sf.FileName);

                    saveChange = false;
                }
                catch (Exception)
                {

                    MessageBox.Show("Error while saving file.");
                }
            }
        }

        /// <summary>
        /// help button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("For Your Help:\n" +
                "draw circle 100\n" +
                "draw rectangle 100 50\n" +
                "draw triangle 10 10 100 10 50 60\n" +
                "draw square 50\n" +
                "move 100 100\n" +
                "color red 23\n" +
                "fill red\n" +
                "fill no\n");
        }

        /// <summary>
        /// Open button click event to load saved files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">click event</param>
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            if (op.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txt_code.Text = File.ReadAllText(op.FileName);
                filePath = Path.GetFullPath(op.FileName);
                saveChange = true;
            }
        }

        /// <summary>
        /// Canvas paint event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnl_canvas_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(outputBitmap, 0, 0);
            
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"> Key down event</param>
        private void txt_code_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string cmm = "run";
                string cod = txt_code.Text.ToLower();

                CommandParser cm = new CommandParser();
                string parsedCode = cm.Parser(cmm, cod);
                if( parsedCode != null)
                {
                    MessageBox.Show(parsedCode);
                }
            }
        }

        
    }
}
