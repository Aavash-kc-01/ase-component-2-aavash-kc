﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GraphicsProgramminApplication
{
    /// <summary>
    /// shape interface
    /// </summary>
    interface Shapes
    {
        /// <summary>
        /// defining method to set color of brush 
        /// </summary>
        /// <param name="c">colour of brush</param>
        void setColor(Color c);
        /// <summary>
        /// defining method to set x-axis position
        /// </summary>
        /// <param name="x">x-axis position</param>
        void setX(int x);
        /// <summary>
        /// defining method to set y-axis position
        /// </summary>
        /// <param name="y">y-axis position</param>
        void setY(int y);

        /// <summary>
        /// defining method to get x-axis position
        /// </summary>
        /// <returns>x-axis position</returns>
        int getX();

        /// <summary>
        /// defining method to get y-axis position
        /// </summary>
        /// <returns>y-axis position</returns>
        int getY();

        /// <summary>
        /// draw method defination
        /// </summary>
        /// <param name="g">Graphics</param>
        /// <param name="c">color of pen</param>
        /// <param name="thickness">thickness of pen</param>
        void draw(Graphics g,Color c, int thickness);
       
    }
}
