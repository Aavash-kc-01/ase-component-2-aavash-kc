﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicsProgramminApplication
{
    /// <summary>
    /// Triangle shape defination class
    /// </summary>
    class Triangle :Shape
    {
        /// <summary>.
        /// Variable declarations
        /// </summary>
        PointF p1, p2, p3;

        /// <summary>
        /// Method to set the int params to pointf variables
        /// </summary>
        /// <param name="point1">x-axis for the first point of triangle</param>
        /// <param name="point2">y-axis for the first point of triangle</param>
        /// <param name="point3">x-axis for the Second point of triangle</param>
        /// <param name="point4">y-axis for the Second point of triangle</param>
        /// <param name="point5">x-axis for the third point of triangle</param>
        /// <param name="point6">y-axis for the third point of triangle</param>
        public void setPoints (int point1,int point2,int point3,int point4, int point5, int point6)
        {
            this.p1 = new PointF(point1, point2);
            this.p2 = new PointF(point3, point4);
            this.p3 = new PointF(point5, point6);
        }

        /// <summary>
        /// draw method for triangle
        /// </summary>
        /// <param name="g">Graphics</param>
        /// <param name="c">Color of border</param>
        /// <param name="thickness">Thickness of border</param>
        public override void draw(Graphics g, Color c, int thickness)
        {
            
            Pen p = new Pen(c, thickness);
            PointF[] curvPoints =
            {
                p1,p2,p3
            };
            g.DrawPolygon(p, curvPoints);
            
        }

       
    }
}
