﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GraphicsProgramminApplication
{
    /// <summary>
    /// Class defination for rectangle shape 
    /// </summary>
    public class Rectangle:Shape
    {
        /// <summary>
        /// variable decleration
        /// </summary>
        int width, height;

        /// <summary>
        /// constructor
        /// </summary>
        public Rectangle() : base()
        {
            width = 100;
            height = 100;
        }

        /// <summary>
        /// Parameterised Constructor
        /// </summary>
        /// <param name="colour">Color of border</param>
        /// <param name="x">x-axis position</param>
        /// <param name="y">y-axis position</param>
        /// <param name
        /// ="width">width of rectangle</param>
        /// <param name="height">height of rectangle</param>
        public Rectangle(Color colour, int x, int y, int width, int height) : base(colour, x, y)
        {

            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// method to set height of rectangle
        /// </summary>
        /// <param name="height">height of rectangel</param>
        public void setHeight(int height)
        {
            this.height = height;
            

        }

        /// <summary>
        /// method to set width of rectangle
        /// </summary>
        /// <param name="width">width of rectangle</param>
        public void setWidth(int width)
        {
            this.width = width;


        }

        /// <summary>
        /// method to get height of rectangle
        /// </summary>
        /// <returns>height of rectangle</returns>
        public int getHeight()
        {
            return height;


        }

        /// <summary>
        /// method to get width of rectangle
        /// </summary>
        /// <returns>width of rectangle</returns>
        public int getWidth()
        {
            return width;


        }


        /// <summary>
        /// draw method for rectangle
        /// </summary>
        /// <param name="g">Graphics </param>
        /// <param name="c"> color of pen</param>
        /// <param name="thickness">thickness of pen</param>
        public override void draw(Graphics g, Color c, int thickness)
        {
            Pen p = new Pen(c, thickness);
            SolidBrush b = new SolidBrush(colour);
            g.FillRectangle(b, x, y, width, height);
            g.DrawRectangle(p, x, y, width, height);
        }

        
    }
}
