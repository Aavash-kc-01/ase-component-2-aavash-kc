﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GraphicsProgramminApplication
{

    /// <summary>
    /// Abstract shape class defination
    /// </summary>
    public abstract class Shape : Shapes
    {
        /// <summary>
        /// variable decleration
        /// </summary>
        protected Color colour; 
        protected int x=0, y=0; 

        /// <summary>
        /// constructor
        /// </summary>
        public Shape()
        {
           
        }

        /// <summary>
        /// Parameterised constructor
        /// </summary>
        /// <param name="colour"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Shape(Color colour, int x, int y)
        {

            this.colour = colour; 
            this.x = x; 
            this.y = y;
            
        }

        /// <summary>
        /// abstract draw method
        /// </summary>
        /// <param name="g">Graphics</param>
        /// <param name="c">color of pen</param>
        /// <param name="thickness">thickness of pen</param>
        public abstract void draw(Graphics g,Color c , int thickness); 
        

        /// <summary>
        /// method to set color for the shape
        /// </summary>
        /// <param name="colour">color of solod brush</param>
        public virtual void setColor(Color colour)
        {
            this.colour = colour;
            
        }

        /// <summary>
        /// method to set the x-axis
        /// </summary>
        /// <param name="x">x-axis position</param>
        public void setX(int x)
        {
            this.x = x;
        }

        /// <summary>
        /// method to set the y-axis
        /// </summary>
        /// <param name="y">y-axis position</param>
        public void setY(int y)
        {
            this.y = y;
        }

        /// <summary>
        /// method to get x-axis
        /// </summary>
        /// <returns>x-axis position</returns>
        public int getX ()
        {
            return x;
        }

        /// <summary>
        /// method to get y-axis
        /// </summary>
        /// <returns>y-axis position</returns>
        public int getY()
        {
            return y;
        }

        public override string ToString()
        {
            return base.ToString() + "    " + this.x + "," + this.y + " : ";
        }

    }
}
