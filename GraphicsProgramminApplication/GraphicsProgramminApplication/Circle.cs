﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GraphicsProgramminApplication
{
    /// <summary>
    /// Circle shape defination class
    /// </summary>
    public class Circle : Shape
    {
        /// <summary>
        /// variable decleration
        /// </summary>
        int radius;

        /// <summary>
        /// Constructor method
        /// </summary>
        public Circle() : base()
        {

        }

        /// <summary>
        /// parameterised constructor
        /// </summary>
        /// <param name="colour">color of pen</param>
        /// <param name="x">x-axis point</param>
        /// <param name="y">y-axis point</param>
        /// <param name="radius">radius of circles</param>
        public Circle(Color colour, int x, int y, int radius) : base(colour, x, y)
        {

            this.radius = radius;
        }

        /// <summary>
        /// method to set radius of the circle
        /// </summary>
        /// <param name="radius"> radius of circle</param>
        public void setRadius(int radius)
        {
            this.radius = radius;


        }

        /// <summary>
        /// Method to return the value of radius
        /// </summary>
        /// <returns> radius of circle</returns>
        public int getRadius()
        {
            return radius;
        }

        /// <summary>
        /// Draw method for a circle
        /// </summary>
        /// <param name="g">Graphics </param>
        /// <param name="c"> color of pen</param>
        /// <param name="thickness">thickness of pen</param>
        public override void draw(Graphics g,Color c, int thickness)
        {

            Pen p = new Pen(c, thickness);
            SolidBrush b = new SolidBrush(colour);
            g.FillEllipse(b, x, y, radius * 2, radius * 2);
            g.DrawEllipse(p, x, y, radius * 2, radius * 2);

        }

     

        public override string ToString() 
        {
            return base.ToString() + "  " + this.radius;
        }
    }
}
