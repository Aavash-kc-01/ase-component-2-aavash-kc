﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicsProgramminApplication
{
    /// <summary>
    /// Shape Factory class defination
    /// </summary>
    public class ShapeFactory
    {

        /// <summary>
        /// Method to get shapes
        /// </summary>
        /// <param name="shapeType">
        /// takes the type of shape in string</param>
        /// <returns>shape object </returns>
        public Shape getShape(String shapeType)
        {
            shapeType = shapeType.ToUpper().Trim(); //you could argue that you want a specific word string to create an object but I'm allowing any case combination


            if (shapeType.Equals("CIRCLE"))
            {
                return new Circle();

            }
            else if (shapeType.Equals("RECTANGLE"))
            {
                return new Rectangle();

            }
            else if (shapeType.Equals("SQUARE"))
            {
                return new Square();
            }
            else if (shapeType.Equals("Triangle"))
            {
                return new Triangle();
            }
            else
            {
                System.ArgumentException argEx = new System.ArgumentException("Factory error: " + shapeType + " does not exist");
                throw argEx;
            }


        }
    }
    
}
