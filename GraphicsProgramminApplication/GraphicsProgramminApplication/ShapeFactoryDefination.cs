﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicsProgramminApplication
{
    /// <summary>
    /// Factory defination class for shapes
    /// </summary>
    public class ShapeFactoryDefination
    {
        /// <summary>
        /// Method to check if the shape is circle
        /// </summary>
        /// <param name="shape"></param>
        /// <returns>true if shape is circle</returns>
        public bool isCircle(string shape)
        {
            if (shape == "circle")
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Method to check if shape is Rectangle
        /// </summary>
        /// <param name="shape"></param>
        /// <returns>true if shape is rectangle</returns>
        public bool isRectangle(string shape)
        {
            if (shape == "rectangle")
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Method to check if a shape is a square
        /// </summary>
        /// <param name="shape"></param>
        /// <returns>true if shape is square</returns>
        public bool isSquare(string shape)
        {
            if (shape == "square")
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Method to check if the shape is a triangle
        /// </summary>
        /// <param name="shape"></param>
        /// <returns>true if shape is triangle</returns>
        public bool isTriangle(string shape)
        {
            if (shape == "triangle")
            {
                return true;
            }
            return false;
        }
    }
}
