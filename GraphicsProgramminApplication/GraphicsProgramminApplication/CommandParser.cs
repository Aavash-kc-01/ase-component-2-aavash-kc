﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicsProgramminApplication
{
    /// <summary>
    /// Class to parse all the command and code written by user
    /// </summary>
    public class CommandParser
    {

        /// <summary>
        /// Variable declerations
        /// </summary>
        int canvasX = 640;
        int canvasY = 480;
        ShapeFactory sF = new ShapeFactory();

        Color c = Color.Black;
        Color fill = Color.Transparent;
        int moveX=0, moveY=0;
        int thickness=3;

        Boolean ifFlag = false;
        Boolean ifCondition = false;
        List<string> ifContent;

        int loopSize;
        Boolean loopFlag = false;
        List<string> loopContent;

        string methodName;
        List<string> methodContent;
        Boolean methodFlag = false;
        Boolean methodCall = false;

        Boolean error = false;
        string error_cmm;
        Bitmap outputBitmap = new Bitmap(640, 480);
        Graphics g;

        int height=0, width=0, size=0, radius=0;
        /// <summary>
        /// Constructor
        /// </summary>
        public CommandParser() {
            outputBitmap = new Bitmap(canvasX, canvasY);
            g =  Graphics.FromImage(outputBitmap);
        }

        /// <summary>
        /// Method to check the operation on if statement
        /// </summary>
        /// <param name="op">Operation from the if statement</param>
        /// <param name="x"> for getting one of the two values to test the operation</param>
        /// <param name="y">for getting one of the two values to test the operation</param>
        /// <returns></returns>
        public static bool Compare(string op, int  x,int y) 
        {
            
                switch (op)
            {
                case "==": return x.CompareTo(y) == 0;
                case "!=": return x.CompareTo(y) != 0;
                case ">": return x.CompareTo(y) > 0;
                case ">=": return x.CompareTo(y) >= 0;
                case "<": return x.CompareTo(y) < 0;
                case "<=": return x.CompareTo(y) <= 0;
                default: throw new ArgumentException("op");
            }
        }

        /// <summary>
        /// method to check individual line codes
        /// </summary>
        /// <param name="code">the line of code in string</param>
        /// <param name="line"> the line number if code</param>
        /// <returns> null if the code works properly and errors if it doesn't</returns>
        public string checkCases(string code,int line)
        {
                int temp;
                
                char[] code_delimiters = new char[] { ' ','(',')' };
                String[] words = code.Split(code_delimiters, StringSplitOptions.RemoveEmptyEntries);

            if (words[0] == "draw")
            {
                if (words[1] == "circle")
                {
                    if (!(words.Length == 3))
                    {
                        error = true;
                        error_cmm = "The error in on line " + line + " please type correct code for 'draw circle'";
                        MessageBox.Show(error_cmm);
                        return error_cmm;
                    }
                    else if (Int32.TryParse(words[2], out temp))
                    {
                        Circle circle = new Circle();
                        circle.setColor(fill);
                        circle.setX(moveX);
                        circle.setY(moveY);
                        circle.setRadius(Convert.ToInt32(words[2]));
                        circle.draw(g, c, thickness);

                    }
                    else if (words[2] == "radius")
                    {
                        Circle circle = new Circle();
                        circle.setColor(fill);
                        circle.setX(moveX);
                        circle.setY(moveY);
                        circle.setRadius(this.radius);
                        circle.draw(g, c, thickness);

                    }
                    else
                    {
                        MessageBox.Show("Please enter The parameters in integers in line " + line);
                        return "Please enter The parameters in integers in line " + line;
                    }
                }
                else if (words[1] == "square")
                {
                    if (!(words.Length == 3))
                    {
                        error = true;
                        error_cmm = "The error in on line " + line + " Please type correct code for 'draw square'";
                        MessageBox.Show(error_cmm);
                        return error_cmm;
                    }
                    else if (Int32.TryParse(words[2], out temp))
                    {
                        Square square = new Square();
                        square.setColor(fill);
                        square.setX(moveX);
                        square.setY(moveY);
                        square.setSize(Convert.ToInt32(words[2]));
                        square.draw(g, c, thickness);

                    }
                    else if (words[2] == "size")
                    {
                        Square square = new Square();
                        square.setColor(fill);
                        square.setX(moveX);
                        square.setY(moveY);
                        square.setSize(this.size);
                        square.draw(g, c, thickness);

                    }
                    else
                    {
                        MessageBox.Show("Please enter The parameters in integers in line " + line);
                        return "Please enter The parameters in integers in line " + line;

                    }
                }

                else if (words[1] == "rectangle")
                {
                    if (!(words.Length == 4))
                    {
                        error = true;
                        error_cmm = "The error in on line " + line + " Please type correct code for 'draw rectangle'";
                        MessageBox.Show(error_cmm);
                        return error_cmm;


                    }
                    else if (Int32.TryParse(words[2], out temp) && Int32.TryParse(words[3], out temp))
                    {
                        Rectangle rectangle = new Rectangle();
                        rectangle.setColor(fill);
                        rectangle.setX(moveX);
                        rectangle.setY(moveY);
                        rectangle.setHeight(Convert.ToInt32(words[2]));
                        rectangle.setWidth(Convert.ToInt32(words[3]));
                        rectangle.draw(g, c, thickness);
                        //return null;
                    }
                    else if ((words[2] == "height" || words[2] == "width") && (words[3] == "height" || words[3] == "width")) {
                        Rectangle rectangle = new Rectangle();
                        rectangle.setColor(fill);
                        rectangle.setX(moveX);
                        rectangle.setY(moveY);
                        rectangle.setHeight(height);
                        rectangle.setWidth(width);
                        rectangle.draw(g, c, thickness);
                    }
                    else if (words[2] == "height" && words[3] != "width")
                    {
                        Rectangle rectangle = new Rectangle();
                        rectangle.setColor(fill);
                        rectangle.setX(moveX);
                        rectangle.setY(moveY);
                        rectangle.setHeight(height);
                        rectangle.setWidth(Convert.ToInt32(words[3]));
                        rectangle.draw(g, c, thickness);
                    }
                    else if (words[3] == "height" && words[2] != "width")
                    {
                        Rectangle rectangle = new Rectangle();
                        rectangle.setColor(fill);
                        rectangle.setX(moveX);
                        rectangle.setY(moveY);
                        rectangle.setHeight(height);
                        rectangle.setWidth(Convert.ToInt32(words[2]));
                        rectangle.draw(g, c, thickness);
                    }
                    else if (words[2] == "width" && words[3] != "height")
                    {
                        Rectangle rectangle = new Rectangle();
                        rectangle.setColor(fill);
                        rectangle.setX(moveX);
                        rectangle.setY(moveY);
                        rectangle.setHeight(width);
                        rectangle.setWidth(Convert.ToInt32(words[3]));
                        rectangle.draw(g, c, thickness);
                    }
                    else
                    {
                        MessageBox.Show("Please enter The parameters in integers in line " + line);
                        return "Please enter The parameters in integers in line " + line;

                    }
                }
                else if (words[1] == "triangle")
                {
                    if (!(words.Length == 8))
                    {
                        error = true;
                        error_cmm = "The error in on line " + line + " Please type correct code for 'draw triangle'";
                        MessageBox.Show(error_cmm);
                        return error_cmm;

                    }
                    else if (Int32.TryParse(words[2], out temp) && Int32.TryParse(words[3], out temp) && Int32.TryParse(words[4], out temp) && Int32.TryParse(words[5], out temp) && Int32.TryParse(words[6], out temp) && Int32.TryParse(words[7], out temp))
                    {
                        Triangle triangle = new Triangle();
                        triangle.setColor(fill);
                        triangle.setPoints(Convert.ToInt32(words[2]), Convert.ToInt32(words[3]), Convert.ToInt32(words[4]), Convert.ToInt32(words[5]), Convert.ToInt32(words[6]), Convert.ToInt32(words[7]));
                        triangle.draw(g, c, thickness);
                        //return null;
                    }
                    else
                    {
                        MessageBox.Show("Please enter The parameters in integers in line " + line);
                        return "Please enter The parameters in integers in line " + line;

                    }
                }
                else
                {
                    MessageBox.Show("Please enter correct code in line " + line);
                    return "Please enter correct code in line " + line;
                }
            }
            else if (words[0] == "move")
            {
                if (!(words.Length == 3))
                {
                    error = true;
                    error_cmm = "The error in on line " + line + " Please type correct code for 'move'";
                    MessageBox.Show(error_cmm);
                    return error_cmm;
                }
                else if (Int32.TryParse(words[1], out temp) && Int32.TryParse(words[2], out temp))
                {
                    this.moveX = Convert.ToInt32(words[1]);
                    this.moveY = Convert.ToInt32(words[2]);
                    //return "Drawing Position changed to " + words[1] + " and " + words[2];
                }
                else
                {
                    MessageBox.Show("Please enter The parameters in integers in line " + line);
                    return "Please enter The parameters in integers in line " + line;

                }
            }

            else if (words[0] == "method")
            {
                this.methodFlag = true;
                this.methodName = words[1];

            }
            else if (words[0] == "endmethod")
            {
                this.methodFlag = false;
            }
            else if (words[0] == this.methodName)
            {

                this.methodCall = true;
            }
            else if (words[0] == "color")
            {
                if (!(words.Length == 3))
                {
                    error = true;
                    error_cmm = "The error in on line " + line + " Please type correct code for 'move'";
                    MessageBox.Show(error_cmm);
                    return error_cmm;
                }
                else if (Int32.TryParse(words[2], out temp))
                {
                    this.thickness = Convert.ToInt32(words[2]);
                    Color cs = Color.FromName(words[1]);

                    if (cs.IsKnownColor)
                    {
                        this.c = Color.FromName(char.ToUpper(words[1][0]) + words[1].Substring(1));

                    }
                    else
                    {
                        MessageBox.Show("Please enter a valid color");
                        return ("please enter a valid color");
                    }
                }
                else
                {
                    MessageBox.Show("Please enter correct parameters");
                    return "Please enter The parameters in integers" + line;

                }
            }

            else if (words[0] == "radius")
            {
                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                {
                    this.radius = Convert.ToInt32(words[2]);
                }
                else if (words[1] == "+=")
                {
                    this.radius = radius + Convert.ToInt32(words[2]);
                }

                else
                {
                    MessageBox.Show("Please enter correct parameters");
                    return "Please enter Correct Parameters";
                }
            }
            else if (words[0] == "height")
            {
                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                {
                    this.height = Convert.ToInt32(words[2]);
                }
                else if (words[1] == "+=")
                {
                    this.height = height + Convert.ToInt32(words[2]);
                }
                else
                {
                    MessageBox.Show("Please enter correct parameters");
                    return "Please enter Correct Parameters";
                }

            }
            else if (words[0] == "width")
            {
                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                {
                    this.width = Convert.ToInt32(words[2]);
                }
                else if (words[1] == "+=")
                {
                    this.width = width + Convert.ToInt32(words[2]);
                }
                else
                {
                    MessageBox.Show("Please enter correct parameters");
                    return "Please enter Correct Parameters";
                }

            }
            else if (words[0] == "size")
            {
                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                {
                    this.size = Convert.ToInt32(words[2]);
                }
                else if (words[1] == "+=")
                {
                    this.size = size + Convert.ToInt32(words[2]);
                }
                else
                {
                    MessageBox.Show("Please enter correct parameters");
                    return "Please enter Correct Parameters";
                }

            }
            else if (words[0] == "fill")
            {
                if (!(words.Length == 2))
                {
                    error = true;
                    error_cmm = "The error in on line " + line + " Please type correct code for 'move'";
                    MessageBox.Show(error_cmm);
                    return error_cmm;
                }

                else
                {
                    Color fil = Color.FromName(words[1]);
                    if (fil.IsKnownColor)
                    {
                        this.fill = Color.FromName(char.ToUpper(words[1][0]) + words[1].Substring(1));
                    }
                    else if (words[1] == "no")
                    {
                        this.fill = Color.Transparent;

                    }
                    else
                    {
                        MessageBox.Show("Please enter a valid color");
                        return ("please enter a valid color");
                    }
                }
            }
            else if (words[0] == "if")
            {
                ifFlag = true;
                if (Int32.TryParse(words[1], out temp) && Int32.TryParse(words[3], out temp))
                {
                    if (words[2] == "==" || words[2] == "!=" || words[2] == ">=" || words[2] == "<=" || words[2] == ">" || words[2] == "<")
                    {
                        if (Compare(words[2], Convert.ToInt32(words[1]), Convert.ToInt32(words[3])))
                        {
                            ifCondition = true;
                            return null;
                        }
                    }
                    else
                    {
                        ifCondition = false;
                        MessageBox.Show("Please enter correct conditions for if statement");
                        return "Error";
                    }
                }
                else if (words[1] == "radius" || words[1] == "size" || words[1] == "height" || words[1] == "width" && Int32.TryParse(words[3], out temp))
                {
                    if (words[1] == "radius")
                    {
                        if (words[2] == "==" || words[2] == "!=" || words[2] == ">=" || words[2] == "<=" || words[2] == ">" || words[2] == "<")
                        {
                            if (Compare(words[2], radius, Convert.ToInt32(words[3])))
                            {
                                ifCondition = true;
                            }

                            else
                            {
                                MessageBox.Show("Please enter correct conditions for if statement");
                                return "Error";
                            }
                        }
                    }
                    else if (words[1] == "size")
                    {
                        if (words[2] == "==" || words[2] == "!=" || words[2] == ">=" || words[2] == "<=" || words[2] == ">" || words[2] == "<")
                        {
                            if (Compare(words[2], size, Convert.ToInt32(words[3])))
                            {
                                ifCondition = true;
                            }

                            else
                            {
                                MessageBox.Show("Please enter correct conditions for if statement");
                                return "Error";
                            }
                        }
                    }
                    else if (words[1] == "height")
                    {
                        if (words[2] == "==" || words[2] == "!=" || words[2] == ">=" || words[2] == "<=" || words[2] == ">" || words[2] == "<")
                        {
                            if (Compare(words[2], height, Convert.ToInt32(words[3])))
                            {
                                ifCondition = true;
                            }

                            else
                            {
                                MessageBox.Show("Please enter correct conditions for if statement");
                                return "Error";
                            }
                        }
                    }
                    else if (words[1] == "width")
                    {
                        if (words[2] == "==" || words[2] == "!=" || words[2] == ">=" || words[2] == "<=" || words[2] == ">" || words[2] == "<")
                        {
                            if (Compare(words[2], width, Convert.ToInt32(words[3])))
                            {
                                ifCondition = true;
                                return null;
                            }

                            else
                            {
                                MessageBox.Show("Please enter correct conditions for if statement");
                                return "Error";
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please enter corrrect variables");
                        return "Please enter corrrect variables";
                    }
                }
            }
            else if (words[0] == "endif")
            {
                ifFlag = false;
                return null;
            }
            else if (words[0] == "loop")
                {
                    if (words.Length == 2)
                    {
                        this.loopFlag = true;
                        this.loopSize = Convert.ToInt32(words[1]);
                    }
                    else
                    {
                        MessageBox.Show("please enter correct parameters");
                        return "please enter correct parameters";
                    }
                }
            else if (words[0] == "endloop")
            {
                loopFlag = false;
                loopSize = 0;
            }
            else
            {
                MessageBox.Show( "Please enter The correct codes");
            }
                
            return null;
        }

        
        /// <summary>
        /// parser method that takes the codes and command form the form
        /// </summary>
        /// <param name="command"> command line string</param>
        /// <param name="code">lines of codes in string</param>
        /// <returns> null if the user input works properly and errors in string if it doesn't</returns>
        public String Parser(string command, string code)
        {
            ShapeFactory sF = new ShapeFactory();
            string comm = command.ToLower();
            char[] code_delimiters = new char[] { ' ' };
            String[] comman = comm.Split(code_delimiters, StringSplitOptions.RemoveEmptyEntries);
            switch (comman[0])
                {
                    case "run":
                    try
                    {
                        char[] delimiters = new char[] { '\r', '\n' };
                        string[] parts = code.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        loopContent = new List<string>();
                        ifContent = new List<string>();
                        methodContent = new List<string>();
                        for (int i = 0; i < parts.Length; i++)
                        {
                            
                            if (this.loopFlag == true)
                            { 
                                     loopContent.Add(parts[i]);
                            }
                            else if (this.methodFlag == true)
                            {
                                if (parts[i] != "endmethod")
                                {
                                    methodContent.Add(parts[i]);
                                }
                                else { checkCases(parts[i], i + 1); }
                            }
                            else if (this.methodFlag == true)
                            {
                                ifContent.Add(parts[i]);
                            }
                            else
                            {
                                
                                checkCases(parts[i], i + 1);
                            }
                            
                        }
                        if (ifCondition == true)
                        {
                            for (int j = 0; j < loopContent.Count - 1; j++)
                            {
                                
                                checkCases(ifContent[j], j);
                            }

                        }
                        if (methodCall == true)
                        {
                            for (int l = 0; l < methodContent.Count - 1; l++)
                            {
                                MessageBox.Show(methodName);
                                checkCases(methodContent[l], l);
                                
                            }
                            methodCall = false;
                        }
                        
                        for (int k = 0; k < loopSize; k++)
                        {
                            for (int j = 0; j < loopContent.Count - 1; j++)
                            {
                                
                                checkCases(loopContent[j], k);
                            }
                            
                        }
                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        return ex.Message;

                    }
                    catch (FormatException ex)
                    {
                        return "!!Please input correct parameter!!";
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        return "!!Please input correct parameter!!";
                    }
                    break;
                case "clear":
                        return "clear";
                    case "reset":
                        this.moveX = 0;
                        this.moveY = 0;
                        this.thickness = 2;
                        this.fill = Color.Transparent;
                        c = Color.Black;
                        return "Pen reset to default value";
                    case "help":
                        return "For Your Help:\n" +
                                 "draw circle 100\n" +
                                 "draw rectangle 100 50\n" +
                                 "draw triangle 10 10 100 10 50 60\n" +
                                 "draw square 50\n" +
                                 "move 100 100\n" +
                                 "color red 23\n" +
                                 "fill red\n" +
                                 "fill no\n";
                    case"":
                        return "Please enter a command";
                    default:
                    checkCases(command, 1);
                    break;

            }
            
            return null;
        }

        /// <summary>
        /// method to get the Bitmap object
        /// </summary>
        /// <returns>the bitmap object that contains all the drawing</returns>
        public Bitmap GetBitmap()
        {
            return this.outputBitmap;
        }
        
    } 
}
