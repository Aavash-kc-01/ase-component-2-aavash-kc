﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GraphicsProgramminApplication
{
    /// <summary>
    /// class defination got square shape
    /// </summary>
    public class Square:Rectangle
    {
        /// <summary>
        /// variable decleration
        /// </summary>
        private int size;

        /// <summary>
        /// constructor
        /// </summary>
        public Square() : base()
        {

        }

        /// <summary>
        /// Parameterised constructor
        /// </summary>
        /// <param name="colour">Color of border</param>
        /// <param name="x">x-axis position</param>
        /// <param name="y">y-axis position</param>
        /// <param name="size">Length of square</param>
        public Square(Color colour, int x, int y, int size) : base(colour, x, y, size, size)
        {
            this.size = size;
        }

        /// <summary>
        /// method to set the size of square
        /// </summary>
        /// <param name="size">length of square</param>
        public void setSize(int size)
        {
            this.size = size;
        }

        /// <summary>
        /// method to get square size
        /// </summary>
        /// <returns>length of square</returns>
        public int getSize()
        {
            return size;
        }

    }
}
