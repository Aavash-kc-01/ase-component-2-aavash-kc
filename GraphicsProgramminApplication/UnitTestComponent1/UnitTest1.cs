﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using GraphicsProgramminApplication;

namespace UnitTestComponent1
{
    /// <summary>
    /// Test Class to test the shape factory defination class
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void IsShapeEquals_Circle_ReturnTrue()
        {
            var shapeFactoryDef = new ShapeFactoryDefination();

            bool result = shapeFactoryDef.isCircle("circle");

            Assert.AreEqual(true, result);
        }
        [TestMethod]
        public void IsShapeEquals_Rectangle_ReturnTrue()
        {
            var shapeFactoryDef = new ShapeFactoryDefination();
            bool result = shapeFactoryDef.isRectangle("rectangle");

            Assert.AreEqual(true, result);
        }
        [TestMethod]
        public void IsShapeEquals_Square_ReturnTrue()
        {
            var shapeFactoryDef = new ShapeFactoryDefination();
            bool result = shapeFactoryDef.isSquare("square");

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void IsShapeEquals_Triangle_ReturnTrue()
        {
            var shapeFactoryDef = new ShapeFactoryDefination();
            bool result = shapeFactoryDef.isTriangle("triangle");

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void testRectangle()
        {
            var r = new Rectangle();
            
            int x = 200, y = 200, size = 100, size1 = 100;
            r.setHeight(size1);
            Assert.AreEqual(100, r.getHeight());
        }

        [TestMethod]
        public void testSquare()
        {
            var s = new Square();
            
            int x = 200, y = 200, size = 100, size1 = 100;
            s.setSize(size1);
            Assert.AreEqual(100, s.getSize());
        }

        [TestMethod]
        public void testCircle()
        {
            var s = new Circle();

            int x = 200, y = 200, radius = 100;
            s.setRadius(radius);
            Assert.AreEqual(100, s.getRadius());
        }


    }
}
